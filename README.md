# New ConTeXt logo

New logo for [ConTeXt](https://wiki.contextgarden.net/Main_Page), a high-quality typesetting program similar to LaTeX.

The logo is available in two color flavors: the standard, multicolored one and a monochromatic version.

Originally the logo came also in a horizontal version that was never used. I removed it in March 2023.

Available in [macOS ICNS](icns/), [PNG](png/), [PDF](pdf/), [SVG](svg/) and [MetaPost](metapost/) format.

The initial announcement and the -- partly controversial -- discussion that followed are accessible on the [ConTeXt mailing list archive](https://www.mail-archive.com/ntg-context@ntg.nl/msg90167.html).

### Default logo

<table>
  <tr>
    <td>![ConTeXt logo](png/ctx_logo_214px.png)</td>
  </tr>
</table>

### Alternative monochromatic version

<table>
  <tr>
    <td>![ConTeXt logo (blue)](png/ctx_logo_blue_214px.png)</td>
  </tr>
</table>

### License

Published in January 2019 under CC0 license (public domain)
